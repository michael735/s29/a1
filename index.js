const express = require('express');


const app = express()	

const PORT = 3000;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.get("/", (req, res) => res.status(200).send("Landing Page") )

app.get("/home", (req, res) => res.status(200).send("Welcome to the home page") )

const  users = [
    {
        "username" : "johndoe",
        "password" : "johndoe1234"
    }
]

app.get("/users", (req, res) => res.status(200).send(users))

app.delete("/delete-user", (req, res) => res.status(200).send(`User ${req.body.username} has been deleted`))

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))
